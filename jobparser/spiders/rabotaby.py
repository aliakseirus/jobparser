import scrapy
from scrapy.http import TextResponse
from jobparser.items import JobparserItem

TEMPLATE_URL = 'https://rabota.by/search/vacancy?text='


class RabotabySpider(scrapy.Spider):
    name = 'rabotaby'
    allowed_domains = ['rabota.by']
    max_page_number = 2
    # start_urls = ['https://rabota.by/vacancies/python-developer']

    def __init__(self, query, area, **kwargs):
        super().__init__()
        self.start_urls = [
            TEMPLATE_URL + query + '&area=' + area
        ]

    def parse_item(self, response: TextResponse):
        title = response.xpath('//h1//text()').get()
        company = response.xpath("//a[contains(@data-qa, 'vacancy-company-name')]/span//text()").get()
        item = JobparserItem()
        item['title'] = title
        item['company'] = company
        item['url'] = response.url
        yield item

    def parse(self, response: TextResponse, page_number: int = 1, **kwargs):
        items = response.xpath("//a[contains(@data-qa, '__vacancy-title')]")
        for item in items[:4]:
            url = item.xpath('./@href').get()
            yield response.follow(url=url, callback=self.parse_item)
        next_url = response.xpath("//a[contains(@data-qa, 'pager-next')]/@href").get()
        # if next_url:
        if next_url and page_number < self.max_page_number:
            new_kwargs = {
                'page_number': page_number + 1
            }
            yield response.follow(url=next_url, callback=self.parse(), cb_kwargs=new_kwargs)
        pass
