import scrapy


class JobparserItem(scrapy.Item):
    title = scrapy.Field()
    company = scrapy.Field()
    url = scrapy.Field()
