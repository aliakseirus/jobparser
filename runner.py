from jobparser.spiders.rabotaby import RabotabySpider
from jobparser import settings

from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings


if __name__ == "__main__":
    crawler_settings = Settings()
    crawler_settings.setmodule(settings)

    process = CrawlerProcess(settings=crawler_settings)

    rabotaby_kwargs = {
        'query': 'python',
        'area': '1002'
    }
    process.crawl(RabotabySpider, **rabotaby_kwargs)

    process.start()
